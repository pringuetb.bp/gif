<?php

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }

    public function getSubCategoriesByMainCategorySlug(string $slugCategory):Query{
        return $this->createQueryBuilder('category')
            ->join('category.parent', 'parent')
            ->where('parent.slug = :slug')
            ->setParameters(['slug'=>$slugCategory])
            ->getQuery()
        ;
        return $query;
    }

    public function getSubCategories():QueryBuilder
    {
        $query = $this->createQueryBuilder('category')
            ->where('category.parent IS NOT NULL')
        ;
        return $query;
    }

    public function  getRecherche(string $recherche):Query{
        return  $this->createQueryBuilder('category')
            ->where('category.slug LIKE :slug')
            ->setParameter('slug', $recherche .'%')
            ->getQuery()
        ;
        return $query;

    }


}
