<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;
use App\Repository\CategoryRepository;

class AppExtension extends AbstractExtension
{
    /*public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('filter_name', [$this, 'doSomething']),
        ];
    }*/

    private  CategoryRepository $categoryRepository;

    public function  __construct(CategoryRepository $categoryRepository){
        $this->categoryRepository = $categoryRepository;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_categories', [$this, 'getCategories']),
            new TwigFunction('get_sous_categories', [$this, 'getSousCategories']),
        ];
    }

    public function getCategories():array
    {
        $result = $this->categoryRepository->findBy(array('parent'=> null));
        //dd($result);
        return $result;
    }
    public function getSousCategories($parent):array
    {
        $result = $this->categoryRepository->findBy(array('parent'=> $parent));
        //dd($result);
        return $result;
    }

}
