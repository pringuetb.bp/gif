<?php

namespace App\Controller\Profile;

use App\Repository\GifRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Controller\Profile;
use App\Entity\Gif;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;
use App\Form\GifType;
use Symfony\Component\String\ByteString;
use Doctrine\ORM\EntityManagerInterface;
use App\Controller\Profile\HomepageController;

class HomepageController extends AbstractController{
    //Anotations => commentaire lu par symfony
    private GifRepository $gifRepository;
    private RequestStack $requestStack;
    private Request $request;
    private EntityManagerInterface $entityManager;
    public function __construct(GifRepository $gifRepository, RequestStack $requestStack, EntityManagerInterface $entityManager){
        $this->gifRepository = $gifRepository;
        $this->requestStack = $requestStack;
        $this->request  = $this -> requestStack -> getCurrentRequest();
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/profile", name="profile.homepage.index")
     */
    public function index():Response
    {

        $user = $this->getUser();

        $gifs = $this->gifRepository->getByUserId( $user->getId() )->getResult();

        return $this->render('profile/index.html.twig',[
            'gifs' => $gifs
        ]);
    }
    /**
     * @Route("/profile/form", name="profile.homepage.form")
    */
    public function form():Response
    {
        $model = new Gif();
        $type = GifType::class;

        $form = $this->createForm($type, $model);
        $form ->handleRequest($this->request);
        if($form->isSubmitted() && $form->isValid()){
            $model->setUser( $this->getUser() );
            $imageName = ByteString::fromRandom(32)->lower();
            $imageExtention = $model->getSource()->guessExtension();
            $model->getSource()->move('img', "$imageName.$imageExtention");
            $model
                ->setSlug("$imageName.$imageExtention")
                ->setSource("$imageName.$imageExtention")
            ;
            $this->entityManager->persist($model);
            $this->entityManager->flush();
            return $this->redirectToRoute('profile.homepage.index');
        }

        return $this->render('profile/form.html.twig',[
            'form' => $form->createView()
        ]);
    }

}
