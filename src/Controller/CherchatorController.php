<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\CategoryRepository;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;

class CherchatorController extends AbstractController{

    private CategoryRepository $categoryRepository;
    private Request $request;
    private RequestStack $requestStack;

    public function __construct(CategoryRepository $categoryRepository, requestStack $requestStack)
    {
        $this->categoryRepository = $categoryRepository;
        $this->requestStack = $requestStack;
        $this->request  = $this -> requestStack -> getCurrentRequest();
    }


    /**
     * @Route("/cherchator", name="cherchator.index")
     */
    public function index():Response
    {
        $recherche = $this->request->query->get('search');
        $rechercheCat = $this->categoryRepository
            ->getRecherche($recherche)
            ->getResult()
        ;

        //dd($rechercheCat);

        return $this->render('cherchator/index.html.twig', [
            'rechercheCat' => $rechercheCat
        ]);
    }
}
