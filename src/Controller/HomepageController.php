<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;

class HomepageController extends AbstractController{
    //Anotations => commentaire lu par symfony

    private RequestStack $requestStack;
    private Request $request;

    public function __construct(RequestStack $requestStack){
        $this->requestStack = $requestStack;
        $this->request  = $this -> requestStack -> getCurrentRequest();
    }

    /**
        * @Route("/", name="homepage.index")
     */
    public function index():Response{
        //dd($this->request->headers->get('accept-language'));
        //$response = new Response('Salut les nazes');
        //return $response;
        return $this->render('homepage/index.html.twig');
    }

    /**
     * @Route("/jesaispas", name="homepage.jesaispas")
     */
    public function jesaispas(){
        return $this->render('homepage/jesaispas.html.twig');
    }
}
