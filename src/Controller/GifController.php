<?php

namespace App\Controller;

use App\Repository\GifRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GifController extends AbstractController{

    private GifRepository $gifRepository;
    public function __construct(GifRepository $gifRepository)
    {
        $this->gifRepository = $gifRepository;
    }

    /**
     * @Route("/gif/{gifSlug}", name="gif.index")
     */
    public function index(string $gifSlug):Response{
        $objgif = $this->gifRepository->findOneBy([
            'slug' => $gifSlug
        ]);
        //dd($objgif);
        return $this->render('gif/index.html.twig', [
            'objgif' => $objgif
        ]);
    }
}
