<?php
namespace App\DataFixtures;

abstract class AbstractDataFixture{
    const CATEGORIES = [
        'animals' =>[
            'Bird',
            'Dog',
            'camel',
        ],
        'actions' =>[
            'Cooking',
            'Eating',
            'Crying',
        ],
    ];
}
