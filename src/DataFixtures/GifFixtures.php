<?php

namespace App\DataFixtures;
use App\Entity\Gif;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\String\Slugger\AsciiSlugger;

class GifFixtures extends Fixture implements DependentFixtureInterface
{
    public function getDependencies():array
    {
        return [
            CategoryFixtures::class,
            UserFixtures::class,
        ];
    }
    public function load(ObjectManager $manager)
    {

        $slugger = new AsciiSlugger();
        foreach (AbstractDataFixture::CATEGORIES as $category => $subcategories)
        {
            foreach ($subcategories as $subcategory)
            {
                $gif = new Gif();
                $gif
                    -> setSource($slugger->slug($subcategory)->lower() . '.gif')
                    -> setSlug($slugger->slug($subcategory)->lower())
                    -> setCategory($this->getReference("subcategory$subcategory"))
                    ->setUser($this->getReference('user'))
                ;

                $manager->persist($gif);
            }
        }
        $manager->flush();
    }
}


