<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\String\Slugger\AsciiSlugger;

class CategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $slugger = new AsciiSlugger();
        foreach (AbstractDataFixture::CATEGORIES as $main => $sub) {
            $mainCategory = new Category();
            $mainCategory
                ->setName($main)
                ->setSlug($slugger->slug($main));

            $manager->persist($mainCategory);

            foreach ($sub as $subCategory) {
                $subCat = new Category();
                $subCat
                    ->setName($subCategory)
                    ->setSlug($slugger->slug($subCategory))
                    ->setParent($mainCategory);

                $manager->persist($subCat);

                $this->addReference("subcategory$subCategory", $subCat);
            }
        }

        $manager->flush();
    }
}
